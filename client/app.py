import requests
import json
import cv2

test_url = 'http://127.0.0.1:5000/api/face_detection'

# กำหนด headers http request
content_type = 'image/jpeg'
headers = {'content-type': content_type}

# เรียกใช้ webcam
cam = cv2.VideoCapture(0)
ret, img = cam.read()
if not ret:
    print("failed to grab image")

# encode image as jpeg
_, img_encoded = cv2.imencode('.jpg', img)
# encode jpeg as bytes
image = img_encoded.tobytes()

# send http request และ receive response
response = requests.post(test_url, image, headers=headers)

print(json.loads(response.text))