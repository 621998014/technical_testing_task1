from flask import Flask, request, Response
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from flask_migrate import Migrate
from redis import Redis
from rq import Queue
from face_detection_engine import use_engine
from model import db

import jsonpickle
import numpy as np
import cv2

# ใช้ flask web framework
app = Flask(__name__)
api = Api(app)
# ใช้ redis และ Redis Queue(rq) ในการจัดการ Queue
r = Redis()
q = Queue(connection=r)

# database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
migrate = Migrate(app, db)


class faceModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String, nullable=False)
    created_at = db.Column(db.DateTime(timezone=True),server_default=func.now())

    def __repr__(self):
        return f'face(image={self.image}, created_at={self.created_at})'

# api
class face_detection(Resource):
    # @marshal_with(resource_field)
    def post(self):
        # convert string of image data to uint8
        nparr = np.fromstring(request.data, np.uint8)
        # decode image
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        # เรียกใช้ engine
        job = q.enqueue(use_engine, img)

        if job.is_finished:
            # faceModel(image=job.result)
            print(job.result)

        # response send back to client
        response = {'message': 'image received.'}
        response_pickled = jsonpickle.encode(response)

        return Response(response=response_pickled, status=200, mimetype="application/json")

# call api
api.add_resource(face_detection, "/api/face_detection")

if __name__ == "__main__":
    app.run(debug=True)
