import cv2
import time
from model import db
from model import faceModel

def face_capture(source):
    # cam = cv2.VideoCapture(0)
    # read image data from input stream
    # image = cv2.imread(source)
    image = source
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image, gray


def webcam_capture(source):
    cam = cv2.VideoCapture(source)

    ret, frame = cam.read()
    if not ret:
        print("failed to grab frame")
        return None, None

    # read image data from input stream
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return frame, gray


def face_detect(gray, cascasdepath="haarcascade_frontalface_default.xml"):
    # process face detection
    face_cascade = cv2.CascadeClassifier(cascasdepath)
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(30, 30)
    )
    print("The number of faces found = ", len(faces))

    return faces, len(faces)


def render(image, faces, nogui=True):
    # create output image
    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x + h, y + h), (0, 255, 0), 2)

    if nogui:
        cv2.imwrite('detected_face.jpg', image)
        
    return image


def use_engine(img):
    print("Task running")
    time.sleep(2)
    Harr = 'haarcascade_frontalface_default.xml'
    print("Using model", Harr)

    # Run the face detection
    image, gray = face_capture(img)

    detected_faces, n_faces = face_detect(gray, cascasdepath=Harr)
    data = render(image, detected_faces, nogui=True)

    faceModel(image=data)

    print("Task complete")
    cv2.destroyAllWindows()
    if n_faces == "0":
        pass
    else:
        pass
    return f'The number of faces found = {n_faces}'
    